#! /usr/bin/env false

use v6.d;

use Template::Prometheus::Metric;

#| This class can hold a collection of Template::Prometheus::Metric objects, to
#| then render into template usable with Prometheus.
unit class Template::Prometheus;

#| A collection of metrics to expose through the template.
has %.metrics;

#| An optional prefix to add to all metrics exposed.
has Str:D $.prefix is default("");

#| The seperator between the prefix and the metric name. Defaults to "_".
has Str:D $.prefix-seperator is default("_");

#| Add a metric to expose. This method returns its own class, allowing it to be
#| chained.
method add-metric (
	#| The metric to expose.
	Template::Prometheus::Metric $metric,

	--> Template::Prometheus
) {
	%!metrics{$metric.name} = $metric;
	self;
}

#| Turn the template into it's stringified form. This renders the template into
#| a usable state, to be scraped by Prometheus.
method Str
{
	%!metrics
		.keys
		.sort
		.map({
			my $metric = %!metrics{$_};
			my $name = $metric.name;

			if $!prefix {
				$name = $!prefix ~ $!prefix-seperator ~ $name;
			}

			qq:to/EOF/
				# HELP $name {$metric.description}
				# TYPE $name {$metric.type}
				$name {$metric.value}
				EOF
		})
		.join
}

=begin pod

=NAME    Template::Prometheus
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
