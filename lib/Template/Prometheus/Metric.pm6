#! /usr/bin/env false

use v6.d;

#| This role defines all the minimal required behaviours of a metric that
#| Prometheus can use.
unit role Template::Prometheus::Metric;

#| The name of the metric.
has $.name is required;

#| The current value of the metric.
has $.value is required;

#| A human-readable description of the metric. This attribute is optional, but
#| highly recommended. Defaults to "An undocumented metric".
has $.description is default("An undocumented metric");

#| A method to expose the type of metric. Simply returns a string.
method type (--> Str) { self.^name.split("::").tail.fc }

=begin pod

=NAME    Template::Prometheus::Metric
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
