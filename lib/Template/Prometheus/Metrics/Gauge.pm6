#! /usr/bin/env false

use v6.d;

use Template::Prometheus::Metric;

#| A Prometheus metric of type "gauge".
unit class Template::Prometheus::Metrics::Gauge is Template::Prometheus::Metric;

=begin pod

=NAME    Template::Prometheus::Metrics::Gauge
=AUTHOR  Patrick Spek <p.spek@tyil.work>
=VERSION 0.1.0

=head1 Synopsis

=head1 Description

=head1 Examples

=head1 See also

=end pod

# vim: ft=perl6 noet
